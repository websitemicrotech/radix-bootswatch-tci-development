(function ($) {
  $(document).ready(function() {

    // add a primary class to form-actions
    $('.form-actions .btn').first().addClass('btn-primary');

  });
})(jQuery);
