<?php
// Plugin definition
$plugin = array(
  'title' => t('Phoenix'),
  'icon' => 'phoenix.png',
  'category' => t('Radix'),
  'theme' => 'phoenix',
  'regions' => array(
    'header' => t('Header'),
    'contentmain' => t('Content'),
    'footer' => t('Footer'),
  ),
);
